## Description

(Describe the feature clearly and concisely.)

## Implementation ideas

(If you have any implementation ideas, they can go here.)
(Any design change proposal could be also discussed on the _to be continuous_ Discord server.)


/label ~"kind/enhancement" ~"status/needs-investigation"
