# [6.4.0](https://gitlab.com/to-be-continuous/python/compare/6.3.5...6.4.0) (2023-11-02)


### Features

* add vault variant ([bf678c4](https://gitlab.com/to-be-continuous/python/commit/bf678c4d300c77f3805b016f9a74a29b4c520af5))

## [6.3.5](https://gitlab.com/to-be-continuous/python/compare/6.3.4...6.3.5) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([d8697c9](https://gitlab.com/to-be-continuous/python/commit/d8697c98a0cf94e471035e518a62202682850508))

## [6.3.4](https://gitlab.com/to-be-continuous/python/compare/6.3.3...6.3.4) (2023-09-22)


### Bug Fixes

* **bumpversion:** fix bumpversion CLI arguments ([7d95ca1](https://gitlab.com/to-be-continuous/python/commit/7d95ca15781e83b8bba05394f095bd5ffc211fc8)), closes [#53](https://gitlab.com/to-be-continuous/python/issues/53)

## [6.3.3](https://gitlab.com/to-be-continuous/python/compare/6.3.2...6.3.3) (2023-08-09)


### Bug Fixes

* add component name to sbom ([6cb2719](https://gitlab.com/to-be-continuous/python/commit/6cb271973e7bdcd2a297b96c16a3a13dd42296be))

## [6.3.2](https://gitlab.com/to-be-continuous/python/compare/6.3.1...6.3.2) (2023-07-07)


### Bug Fixes

* **package:** expire built artifacts ([d4f7b4a](https://gitlab.com/to-be-continuous/python/commit/d4f7b4aa24c2ae220b98fa56d66aaafe5794fbc7))

## [6.3.1](https://gitlab.com/to-be-continuous/python/compare/6.3.0...6.3.1) (2023-06-27)


### Bug Fixes

* **publish:** avoid rebuilding packages (optimization) ([0002705](https://gitlab.com/to-be-continuous/python/commit/0002705aa22716db026a90f6a899ef786efcbe35))

# [6.3.0](https://gitlab.com/to-be-continuous/python/compare/6.2.0...6.3.0) (2023-05-28)


### Features

* **release:** implement 2 steps release ([1a58dd1](https://gitlab.com/to-be-continuous/python/commit/1a58dd1908c6a210ff9a285bc2a61b0fdcb1410b))
* **release:** make release commit message configurable ([2eba9cf](https://gitlab.com/to-be-continuous/python/commit/2eba9cf352099a0cefa930d59ff995e8dca06d56))

# [6.2.0](https://gitlab.com/to-be-continuous/python/compare/6.1.5...6.2.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([ba47f0e](https://gitlab.com/to-be-continuous/python/commit/ba47f0ee543aef7f80f33a10e023c5f214e3b7b1))

## [6.1.5](https://gitlab.com/to-be-continuous/python/compare/6.1.4...6.1.5) (2023-03-28)


### Bug Fixes

* **sbom:** add CycloneDX report ([6525c1c](https://gitlab.com/to-be-continuous/python/commit/6525c1cf39fe2e88039122a523bc96e6b1903431))

## [6.1.4](https://gitlab.com/to-be-continuous/python/compare/6.1.3...6.1.4) (2023-03-18)


### Bug Fixes

* **bumpversion:** use bumpversion configuration in priority if cfg file found ([cd4abb3](https://gitlab.com/to-be-continuous/python/commit/cd4abb30aa8670de948c5a0e76f3ccff9144436e))

## [6.1.3](https://gitlab.com/to-be-continuous/python/compare/6.1.2...6.1.3) (2023-01-29)


### Bug Fixes

* replace wget and curl by python scripts ([ef78ec7](https://gitlab.com/to-be-continuous/python/commit/ef78ec7d44549cd2909065ef767de9740ed0de82))

## [6.1.2](https://gitlab.com/to-be-continuous/python/compare/6.1.1...6.1.2) (2023-01-28)


### Bug Fixes

* PYTHON_PROJECT_DIR support for py-sbom ([2e7dc62](https://gitlab.com/to-be-continuous/python/commit/2e7dc62ea4d91ac4af80ca3652547992db71c966))

## [6.1.1](https://gitlab.com/to-be-continuous/python/compare/6.1.0...6.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([5650f7e](https://gitlab.com/to-be-continuous/python/commit/5650f7e213814fa6b5e78353d089fd3aac24f7d5))

# [6.1.0](https://gitlab.com/to-be-continuous/python/compare/6.0.2...6.1.0) (2022-12-13)


### Features

* add a job generating software bill of materials ([4c56888](https://gitlab.com/to-be-continuous/python/commit/4c56888e589c11ac60edf79f2aa9dcd23bda9062))

## [6.0.2](https://gitlab.com/to-be-continuous/python/compare/6.0.1...6.0.2) (2022-10-17)


### Bug Fixes

* bumpversion when using only setup.py. Refs: [#36](https://gitlab.com/to-be-continuous/python/issues/36) ([2944687](https://gitlab.com/to-be-continuous/python/commit/294468773ed6f60eaf93ee345047a65bcd4c78b4))

## [6.0.1](https://gitlab.com/to-be-continuous/python/compare/6.0.0...6.0.1) (2022-10-04)


### Bug Fixes

* **bandit:** fix shell syntax error ([bb64f96](https://gitlab.com/to-be-continuous/python/commit/bb64f96f228f13da9204567cb45a1d62d53ca121))

# [6.0.0](https://gitlab.com/to-be-continuous/python/compare/5.1.0...6.0.0) (2022-10-04)


### Features

* normalize reports ([d591f6d](https://gitlab.com/to-be-continuous/python/commit/d591f6d1f2e5469dc8e926e2860f25516e64a820))


### BREAKING CHANGES

* generated reports have changed (see doc). It is a breaking change if you're using SonarQube.

# [5.1.0](https://gitlab.com/to-be-continuous/python/compare/5.0.0...5.1.0) (2022-09-11)


### Features

* add ability to setup build tool version in PYTHON_BUILD_SYSTEM ([5bea2dd](https://gitlab.com/to-be-continuous/python/commit/5bea2dd8e9e4e50cdf6f0c7c6ef882e82b97c7d6))

# [5.0.0](https://gitlab.com/to-be-continuous/python/compare/4.2.0...5.0.0) (2022-08-05)


### Features

* adaptive pipeline rules ([543b4fe](https://gitlab.com/to-be-continuous/python/commit/543b4fe6e80ff0921d08a64f412308c128708a4d))
* switch to Merge Request pipelines as default ([714e066](https://gitlab.com/to-be-continuous/python/commit/714e066c2e4e33a5e109446c410e50f86e32f899))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [4.2.0](https://gitlab.com/to-be-continuous/python/compare/4.1.1...4.2.0) (2022-06-10)


### Features

* **lint:** add a report for SonarQube ([ba73998](https://gitlab.com/to-be-continuous/python/commit/ba7399884688452762d5d2d873f6ae82ab026a50))

## [4.1.1](https://gitlab.com/to-be-continuous/python/compare/4.1.0...4.1.1) (2022-05-06)


### Bug Fixes

* Manage deprecation for artifacts:report:cobertura ([9d7bcf3](https://gitlab.com/to-be-continuous/python/commit/9d7bcf393212c73ed16dd306abcd5a82ff04e5e5))

# [4.1.0](https://gitlab.com/to-be-continuous/python/compare/4.0.1...4.1.0) (2022-05-01)


### Bug Fixes

* migrate deprecated CI_BUILD_REF_NAME variable ([eb115a2](https://gitlab.com/to-be-continuous/python/commit/eb115a2391cb582f50fdec9564c177b1d71c2fa7))


### Features

* configurable tracking image ([2a0229f](https://gitlab.com/to-be-continuous/python/commit/2a0229fe98e0513d6847ddc621d1f5f8cb0cc1b1))

## [4.0.1](https://gitlab.com/to-be-continuous/python/compare/4.0.0...4.0.1) (2022-03-22)


### Bug Fixes

* **Trivy:** Scan transitive dependencies ([11d96db](https://gitlab.com/to-be-continuous/python/commit/11d96db74d03e157927177dd87438e9575111086))

# [4.0.0](https://gitlab.com/to-be-continuous/python/compare/3.2.1...4.0.0) (2022-02-25)


### Bug Fixes

* **Poetry:** Poetry cache in GitLab CI cache ([9fbaa6d](https://gitlab.com/to-be-continuous/python/commit/9fbaa6db687746c0c223caf01bf745f7eac91abb))


### chore

* renamed unprefixed variables ([8c8a873](https://gitlab.com/to-be-continuous/python/commit/8c8a873b795c4f8a6a8f07e9ed7729d9c35dacd5))


### Features

* add multi build-system support (Poetry, Setuptools or requirements file) ([130e210](https://gitlab.com/to-be-continuous/python/commit/130e2102af56dc8719ba5c87a7e31902fb9fe228))
* add Pipenv support ([7afc0db](https://gitlab.com/to-be-continuous/python/commit/7afc0dbfccfe6b7678cce2d6a9f7f7ececff193f))
* **release:** complete release process refactoring ([ff8b985](https://gitlab.com/to-be-continuous/python/commit/ff8b9856a0bb045932f4810410404261cd848ea4))


### BREAKING CHANGES

* **release:** complete refactoring or release process, including variables and jobs redefinition
- no more separate publish job: the entire release process is now performed by the py-release job
- TWINE_XXX variables removed and replaced by PYTHON_REPOSITORY_XXX
- RELEASE_VERSION_PART variable replaced by PYTHON_RELEASE_NEXT

For additional info, see the doc.
* rename $REQUIREMENTS_FILE as $PYTHON_REQS_FILE and $PYTHON_REQS_FILE as $PYTHON_EXTRA_REQS_FILES
default extra requirements changed from 'test-requirements.txt' to 'requirements-dev.txt'
* removed $PYTHON_POETRY_DISABLED with $PYTHON_BUILD_SYSTEM (see doc)

## [3.2.1](https://gitlab.com/to-be-continuous/python/compare/3.2.0...3.2.1) (2021-12-21)


### Bug Fixes

* **safety:** fix command not found when poetry is used ([1ee673b](https://gitlab.com/to-be-continuous/python/commit/1ee673b8e323d4e5733ae3cf91eb74fdc5393a9e))

# [3.2.0](https://gitlab.com/to-be-continuous/python/compare/3.1.1...3.2.0) (2021-12-20)


### Features

* add Trivy dependency scanner ([f0faed0](https://gitlab.com/to-be-continuous/python/commit/f0faed09819de9b45d748757d75bbf7680a69bce))

## [3.1.1](https://gitlab.com/to-be-continuous/python/compare/3.1.0...3.1.1) (2021-12-17)


### Bug Fixes

* switch from safety image to install safety ([e2b42c4](https://gitlab.com/to-be-continuous/python/commit/e2b42c407cf70ab9967977bbbfe745f6547a6ca1))

# [3.1.0](https://gitlab.com/to-be-continuous/python/compare/3.0.1...3.1.0) (2021-12-10)


### Features

* **publish:** configure the GitLab Packages registry as default Python registry for publish ([891c32a](https://gitlab.com/to-be-continuous/python/commit/891c32aecc986599417ff2404f83eaff66ee4400))

## [3.0.1](https://gitlab.com/to-be-continuous/python/compare/3.0.0...3.0.1) (2021-12-10)


### Bug Fixes

* preserve explicit project dependencies versions when installing tools ([c0c9464](https://gitlab.com/to-be-continuous/python/commit/c0c9464782c71f1fa67d3ddb14ae437b17228a06))

# [3.0.0](https://gitlab.com/to-be-continuous/python/compare/2.2.0...3.0.0) (2021-11-20)


### Features

* fully integration of poetry ([f0406de](https://gitlab.com/to-be-continuous/python/commit/f0406debc207728ee5ea6c71e42357ad965f7c6a))


### refacto

* **py-doc:** remove Python doc build ([10a8150](https://gitlab.com/to-be-continuous/python/commit/10a8150e1d9d43f42458846e13aec19db68cccd8))


### BREAKING CHANGES

* **py-doc:** doc job removed
this job has to been rewritten :
	- it is in a wrong stage
	- needs an other tool (make)
	- generated doc is not publish anywhere
	- no ability to choise doc generation tool

in to-be-continuous, there is mkdocs template which is able to generate python doc too

# Conflicts:
#	templates/gitlab-ci-python.yml

# [2.2.0](https://gitlab.com/to-be-continuous/python/compare/2.1.1...2.2.0) (2021-11-15)


### Features

* move packaging to a separate stage ([945fc8a](https://gitlab.com/to-be-continuous/python/commit/945fc8a8c9d298640de2860b15c7162b1ad33684))

## [2.1.1](https://gitlab.com/to-be-continuous/python/compare/2.1.0...2.1.1) (2021-11-09)


### Bug Fixes

* Use PIP_OPTS for setup.py based install ([3ea29e6](https://gitlab.com/to-be-continuous/python/commit/3ea29e634c1e1a20a1f6f84f56ed8881e570d7c2))

## [2.0.3](https://gitlab.com/to-be-continuous/python/compare/2.0.2...2.0.3) (2021-10-12)


### Bug Fixes

* disable poetry usage (py-doc) ([73d5f2a](https://gitlab.com/to-be-continuous/python/commit/73d5f2a024f72c0b28b4ef10895bd8113ff7f932))

## [2.0.2](https://gitlab.com/to-be-continuous/python/compare/2.0.1...2.0.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([77af297](https://gitlab.com/to-be-continuous/python/commit/77af297de4d99257ee286d07b1d2837948887ac5))

## [2.0.1](https://gitlab.com/to-be-continuous/python/compare/2.0.0...2.0.1) (2021-10-04)


### Bug Fixes

* disable poetry usage ([17d57cb](https://gitlab.com/to-be-continuous/python/commit/17d57cb9626de30be281c147486745df78f78545))

## [2.0.0](https://gitlab.com/to-be-continuous/python/compare/1.3.0...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([4bb11b9](https://gitlab.com/to-be-continuous/python/commit/4bb11b9e2f971ba75fe1c4a36b9c1d475a6f1cb6))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.3.0](https://gitlab.com/to-be-continuous/python/compare/1.2.3...1.3.0) (2021-09-03)

### Features

* add Poetry extras support (PYTHON_POETRY_EXTRAS variable) ([e079e30](https://gitlab.com/to-be-continuous/python/commit/e079e305ddaf508d3105394df3fab1be92d6e38c))

## [1.2.3](https://gitlab.com/to-be-continuous/python/compare/1.2.2...1.2.3) (2021-07-26)

### Bug Fixes

* **poetry:** add option to disable poetry ([dbfe6f6](https://gitlab.com/to-be-continuous/python/commit/dbfe6f6b9abee4bf4aa68b603d015283a5e0bcc1))

## [1.2.2](https://gitlab.com/to-be-continuous/python/compare/1.2.1...1.2.2) (2021-06-24)

### Bug Fixes

* permission on reports directory ([f44e03a](https://gitlab.com/to-be-continuous/python/commit/f44e03a3afbad8c68babf51bc883da52bdf1c5b7))

## [1.2.1](https://gitlab.com/to-be-continuous/python/compare/1.2.0...1.2.1) (2021-06-23)

### Bug Fixes

* \"Missing git package for py-release job\" ([082f308](https://gitlab.com/to-be-continuous/python/commit/082f308330eb16a42704370190c66ba5a7823671))

## [1.2.0](https://gitlab.com/to-be-continuous/python/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([688d6f2](https://gitlab.com/to-be-continuous/python/commit/688d6f26374c4bc0610a0f979ed836c5e46c7754))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/python/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([73dbac6](https://gitlab.com/Orange-OpenSource/tbc/python/commit/73dbac6b81dcbe22b3fcfdbd34493b43fe8464a2))

## 1.0.0 (2021-05-06)

### Features

* initial release ([a1a8677](https://gitlab.com/Orange-OpenSource/tbc/python/commit/a1a867713c55fdc0ac17c3e7bd5540a7aee314cd))
